
package com.faidayetu.wsdlPackage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.faidayetu.wsdlPackage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LoanApplicationsRoot_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50035", "LoanApplicationsRoot");
    private final static QName _MemberscompanyRoot_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50039", "MemberscompanyRoot");
    private final static QName _LoanChargesCodesRoot_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50037", "LoanChargesCodesRoot");
    private final static QName _Root_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50034", "Root");
    private final static QName _LoanChargesRoot_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50036", "LoanChargesRoot");
    private final static QName _MemberDetails_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50038", "MemberDetails");
    private final static QName _MemberDetailsMember_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50038", "Member");
    private final static QName _RootLoanProductTypes_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50034", "LoanProductTypes");
    private final static QName _MemberscompanyRootMembersCompany_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50039", "MembersCompany");
    private final static QName _LoanApplicationsRootLoanApplications_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50035", "LoanApplications");
    private final static QName _LoanChargesCodesRootLoanCharges_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50037", "LoanCharges");
    private final static QName _LoanChargesRootLoanProcessingCharges_QNAME = new QName("urn:microsoft-dynamics-nav/xmlports/x50036", "LoanProcessingCharges");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.faidayetu.wsdlPackage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MemberscompanyRoot }
     * 
     */
    public MemberscompanyRoot createMemberscompanyRoot() {
        return new MemberscompanyRoot();
    }

    /**
     * Create an instance of {@link MembersCompany }
     * 
     */
    public MembersCompany createMembersCompany() {
        return new MembersCompany();
    }

    /**
     * Create an instance of {@link GetLoanProcessingChargesResult }
     * 
     */
    public GetLoanProcessingChargesResult createGetLoanProcessingChargesResult() {
        return new GetLoanProcessingChargesResult();
    }

    /**
     * Create an instance of {@link LoanChargesRoot }
     * 
     */
    public LoanChargesRoot createLoanChargesRoot() {
        return new LoanChargesRoot();
    }

    /**
     * Create an instance of {@link GetMemberDetailsResult }
     * 
     */
    public GetMemberDetailsResult createGetMemberDetailsResult() {
        return new GetMemberDetailsResult();
    }

    /**
     * Create an instance of {@link MemberDetails }
     * 
     */
    public MemberDetails createMemberDetails() {
        return new MemberDetails();
    }

    /**
     * Create an instance of {@link GetLoanChargesResult }
     * 
     */
    public GetLoanChargesResult createGetLoanChargesResult() {
        return new GetLoanChargesResult();
    }

    /**
     * Create an instance of {@link LoanChargesCodesRoot }
     * 
     */
    public LoanChargesCodesRoot createLoanChargesCodesRoot() {
        return new LoanChargesCodesRoot();
    }

    /**
     * Create an instance of {@link GetMembercompany }
     * 
     */
    public GetMembercompany createGetMembercompany() {
        return new GetMembercompany();
    }

    /**
     * Create an instance of {@link GetLoanProducts }
     * 
     */
    public GetLoanProducts createGetLoanProducts() {
        return new GetLoanProducts();
    }

    /**
     * Create an instance of {@link Root }
     * 
     */
    public Root createRoot() {
        return new Root();
    }

    /**
     * Create an instance of {@link GetMemberDetails }
     * 
     */
    public GetMemberDetails createGetMemberDetails() {
        return new GetMemberDetails();
    }

    /**
     * Create an instance of {@link GetLoanApplicationsPerMemberResult }
     * 
     */
    public GetLoanApplicationsPerMemberResult createGetLoanApplicationsPerMemberResult() {
        return new GetLoanApplicationsPerMemberResult();
    }

    /**
     * Create an instance of {@link LoanApplicationsRoot }
     * 
     */
    public LoanApplicationsRoot createLoanApplicationsRoot() {
        return new LoanApplicationsRoot();
    }

    /**
     * Create an instance of {@link ModifyLoanApplication }
     * 
     */
    public ModifyLoanApplication createModifyLoanApplication() {
        return new ModifyLoanApplication();
    }

    /**
     * Create an instance of {@link ModifyLoanApplicationResult }
     * 
     */
    public ModifyLoanApplicationResult createModifyLoanApplicationResult() {
        return new ModifyLoanApplicationResult();
    }

    /**
     * Create an instance of {@link CreateLoanApplication }
     * 
     */
    public CreateLoanApplication createCreateLoanApplication() {
        return new CreateLoanApplication();
    }

    /**
     * Create an instance of {@link GetLoanProductsResult }
     * 
     */
    public GetLoanProductsResult createGetLoanProductsResult() {
        return new GetLoanProductsResult();
    }

    /**
     * Create an instance of {@link CreateLoanApplicationResult }
     * 
     */
    public CreateLoanApplicationResult createCreateLoanApplicationResult() {
        return new CreateLoanApplicationResult();
    }

    /**
     * Create an instance of {@link GetLoanProcessingCharges }
     * 
     */
    public GetLoanProcessingCharges createGetLoanProcessingCharges() {
        return new GetLoanProcessingCharges();
    }

    /**
     * Create an instance of {@link GetLoanCharges }
     * 
     */
    public GetLoanCharges createGetLoanCharges() {
        return new GetLoanCharges();
    }

    /**
     * Create an instance of {@link GetLoanApplicationsPerMember }
     * 
     */
    public GetLoanApplicationsPerMember createGetLoanApplicationsPerMember() {
        return new GetLoanApplicationsPerMember();
    }

    /**
     * Create an instance of {@link GetMembercompanyResult }
     * 
     */
    public GetMembercompanyResult createGetMembercompanyResult() {
        return new GetMembercompanyResult();
    }

    /**
     * Create an instance of {@link GetLoanApplications }
     * 
     */
    public GetLoanApplications createGetLoanApplications() {
        return new GetLoanApplications();
    }

    /**
     * Create an instance of {@link GetLoanApplicationsResult }
     * 
     */
    public GetLoanApplicationsResult createGetLoanApplicationsResult() {
        return new GetLoanApplicationsResult();
    }

    /**
     * Create an instance of {@link LoanProductTypes }
     * 
     */
    public LoanProductTypes createLoanProductTypes() {
        return new LoanProductTypes();
    }

    /**
     * Create an instance of {@link LoanApplications }
     * 
     */
    public LoanApplications createLoanApplications() {
        return new LoanApplications();
    }

    /**
     * Create an instance of {@link LoanProcessingCharges }
     * 
     */
    public LoanProcessingCharges createLoanProcessingCharges() {
        return new LoanProcessingCharges();
    }

    /**
     * Create an instance of {@link LoanCharges }
     * 
     */
    public LoanCharges createLoanCharges() {
        return new LoanCharges();
    }

    /**
     * Create an instance of {@link Member }
     * 
     */
    public Member createMember() {
        return new Member();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanApplicationsRoot }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50035", name = "LoanApplicationsRoot")
    public JAXBElement<LoanApplicationsRoot> createLoanApplicationsRoot(LoanApplicationsRoot value) {
        return new JAXBElement<LoanApplicationsRoot>(_LoanApplicationsRoot_QNAME, LoanApplicationsRoot.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberscompanyRoot }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50039", name = "MemberscompanyRoot")
    public JAXBElement<MemberscompanyRoot> createMemberscompanyRoot(MemberscompanyRoot value) {
        return new JAXBElement<MemberscompanyRoot>(_MemberscompanyRoot_QNAME, MemberscompanyRoot.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanChargesCodesRoot }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50037", name = "LoanChargesCodesRoot")
    public JAXBElement<LoanChargesCodesRoot> createLoanChargesCodesRoot(LoanChargesCodesRoot value) {
        return new JAXBElement<LoanChargesCodesRoot>(_LoanChargesCodesRoot_QNAME, LoanChargesCodesRoot.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Root }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50034", name = "Root")
    public JAXBElement<Root> createRoot(Root value) {
        return new JAXBElement<Root>(_Root_QNAME, Root.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanChargesRoot }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50036", name = "LoanChargesRoot")
    public JAXBElement<LoanChargesRoot> createLoanChargesRoot(LoanChargesRoot value) {
        return new JAXBElement<LoanChargesRoot>(_LoanChargesRoot_QNAME, LoanChargesRoot.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MemberDetails }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50038", name = "MemberDetails")
    public JAXBElement<MemberDetails> createMemberDetails(MemberDetails value) {
        return new JAXBElement<MemberDetails>(_MemberDetails_QNAME, MemberDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Member }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50038", name = "Member", scope = MemberDetails.class)
    public JAXBElement<Member> createMemberDetailsMember(Member value) {
        return new JAXBElement<Member>(_MemberDetailsMember_QNAME, Member.class, MemberDetails.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanProductTypes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50034", name = "LoanProductTypes", scope = Root.class)
    public JAXBElement<LoanProductTypes> createRootLoanProductTypes(LoanProductTypes value) {
        return new JAXBElement<LoanProductTypes>(_RootLoanProductTypes_QNAME, LoanProductTypes.class, Root.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MembersCompany }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50039", name = "MembersCompany", scope = MemberscompanyRoot.class)
    public JAXBElement<MembersCompany> createMemberscompanyRootMembersCompany(MembersCompany value) {
        return new JAXBElement<MembersCompany>(_MemberscompanyRootMembersCompany_QNAME, MembersCompany.class, MemberscompanyRoot.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanApplications }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50035", name = "LoanApplications", scope = LoanApplicationsRoot.class)
    public JAXBElement<LoanApplications> createLoanApplicationsRootLoanApplications(LoanApplications value) {
        return new JAXBElement<LoanApplications>(_LoanApplicationsRootLoanApplications_QNAME, LoanApplications.class, LoanApplicationsRoot.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanCharges }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50037", name = "LoanCharges", scope = LoanChargesCodesRoot.class)
    public JAXBElement<LoanCharges> createLoanChargesCodesRootLoanCharges(LoanCharges value) {
        return new JAXBElement<LoanCharges>(_LoanChargesCodesRootLoanCharges_QNAME, LoanCharges.class, LoanChargesCodesRoot.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanProcessingCharges }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:microsoft-dynamics-nav/xmlports/x50036", name = "LoanProcessingCharges", scope = LoanChargesRoot.class)
    public JAXBElement<LoanProcessingCharges> createLoanChargesRootLoanProcessingCharges(LoanProcessingCharges value) {
        return new JAXBElement<LoanProcessingCharges>(_LoanChargesRootLoanProcessingCharges_QNAME, LoanProcessingCharges.class, LoanChargesRoot.class, value);
    }

}
