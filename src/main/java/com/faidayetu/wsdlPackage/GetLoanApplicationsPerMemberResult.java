
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exportLoanApplications" type="{urn:microsoft-dynamics-nav/xmlports/x50035}LoanApplicationsRoot"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exportLoanApplications"
})
@XmlRootElement(name = "GetLoanApplicationsPerMember_Result")
public class GetLoanApplicationsPerMemberResult {

    @XmlElement(required = true)
    protected LoanApplicationsRoot exportLoanApplications;

    /**
     * Gets the value of the exportLoanApplications property.
     * 
     * @return
     *     possible object is
     *     {@link LoanApplicationsRoot }
     *     
     */
    public LoanApplicationsRoot getExportLoanApplications() {
        return exportLoanApplications;
    }

    /**
     * Sets the value of the exportLoanApplications property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanApplicationsRoot }
     *     
     */
    public void setExportLoanApplications(LoanApplicationsRoot value) {
        this.exportLoanApplications = value;
    }

}
