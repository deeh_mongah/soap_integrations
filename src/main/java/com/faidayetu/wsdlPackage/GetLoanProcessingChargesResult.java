
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exportLoanProcessingcharges" type="{urn:microsoft-dynamics-nav/xmlports/x50036}LoanChargesRoot"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exportLoanProcessingcharges"
})
@XmlRootElement(name = "GetLoanProcessingCharges_Result")
public class GetLoanProcessingChargesResult {

    @XmlElement(required = true)
    protected LoanChargesRoot exportLoanProcessingcharges;

    /**
     * Gets the value of the exportLoanProcessingcharges property.
     * 
     * @return
     *     possible object is
     *     {@link LoanChargesRoot }
     *     
     */
    public LoanChargesRoot getExportLoanProcessingcharges() {
        return exportLoanProcessingcharges;
    }

    /**
     * Sets the value of the exportLoanProcessingcharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanChargesRoot }
     *     
     */
    public void setExportLoanProcessingcharges(LoanChargesRoot value) {
        this.exportLoanProcessingcharges = value;
    }

}
