
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanApplications complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanApplications">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="LoanNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplicationDate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Membercode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RequestedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApprovedAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Interest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Insurance" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MemberName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Installments" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ModeOfDisbursement" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InstallementPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Repayment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanProductTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RepaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MonthlyRepayment" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MonthlyInterest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SmsDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Eloan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PayrollNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoansPurpose" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanApplications", namespace = "urn:microsoft-dynamics-nav/xmlports/x50035", propOrder = {
    "loanNo",
    "applicationDate",
    "loanProductType",
    "membercode",
    "companyCode",
    "requestedAmount",
    "approvedAmount",
    "interest",
    "insurance",
    "memberName",
    "installments",
    "modeOfDisbursement",
    "installementPeriod",
    "repayment",
    "loanProductTypeName",
    "repaymentMethod",
    "monthlyRepayment",
    "monthlyInterest",
    "smsDescription",
    "eloan",
    "payrollNo",
    "loansPurpose"
})
public class LoanApplications {

    @XmlElement(name = "LoanNo", required = true)
    protected String loanNo;
    @XmlElement(name = "ApplicationDate", required = true)
    protected String applicationDate;
    @XmlElement(name = "LoanProductType", required = true)
    protected String loanProductType;
    @XmlElement(name = "Membercode", required = true)
    protected String membercode;
    @XmlElement(name = "CompanyCode", required = true)
    protected String companyCode;
    @XmlElement(name = "RequestedAmount", required = true)
    protected String requestedAmount;
    @XmlElement(name = "ApprovedAmount", required = true)
    protected String approvedAmount;
    @XmlElement(name = "Interest", required = true)
    protected String interest;
    @XmlElement(name = "Insurance", required = true)
    protected String insurance;
    @XmlElement(name = "MemberName", required = true)
    protected String memberName;
    @XmlElement(name = "Installments", defaultValue = "0")
    protected int installments;
    @XmlElement(name = "ModeOfDisbursement", required = true)
    protected String modeOfDisbursement;
    @XmlElement(name = "InstallementPeriod", required = true)
    protected String installementPeriod;
    @XmlElement(name = "Repayment", required = true)
    protected String repayment;
    @XmlElement(name = "LoanProductTypeName", required = true)
    protected String loanProductTypeName;
    @XmlElement(name = "RepaymentMethod", required = true)
    protected String repaymentMethod;
    @XmlElement(name = "MonthlyRepayment", required = true)
    protected String monthlyRepayment;
    @XmlElement(name = "MonthlyInterest", required = true)
    protected String monthlyInterest;
    @XmlElement(name = "SmsDescription", required = true)
    protected String smsDescription;
    @XmlElement(name = "Eloan", required = true)
    protected String eloan;
    @XmlElement(name = "PayrollNo", required = true)
    protected String payrollNo;
    @XmlElement(name = "LoansPurpose", required = true)
    protected String loansPurpose;

    /**
     * Gets the value of the loanNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanNo() {
        return loanNo;
    }

    /**
     * Sets the value of the loanNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanNo(String value) {
        this.loanNo = value;
    }

    /**
     * Gets the value of the applicationDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationDate() {
        return applicationDate;
    }

    /**
     * Sets the value of the applicationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationDate(String value) {
        this.applicationDate = value;
    }

    /**
     * Gets the value of the loanProductType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanProductType() {
        return loanProductType;
    }

    /**
     * Sets the value of the loanProductType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanProductType(String value) {
        this.loanProductType = value;
    }

    /**
     * Gets the value of the membercode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMembercode() {
        return membercode;
    }

    /**
     * Sets the value of the membercode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMembercode(String value) {
        this.membercode = value;
    }

    /**
     * Gets the value of the companyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the value of the companyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCode(String value) {
        this.companyCode = value;
    }

    /**
     * Gets the value of the requestedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestedAmount() {
        return requestedAmount;
    }

    /**
     * Sets the value of the requestedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestedAmount(String value) {
        this.requestedAmount = value;
    }

    /**
     * Gets the value of the approvedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovedAmount() {
        return approvedAmount;
    }

    /**
     * Sets the value of the approvedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovedAmount(String value) {
        this.approvedAmount = value;
    }

    /**
     * Gets the value of the interest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterest() {
        return interest;
    }

    /**
     * Sets the value of the interest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterest(String value) {
        this.interest = value;
    }

    /**
     * Gets the value of the insurance property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurance() {
        return insurance;
    }

    /**
     * Sets the value of the insurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurance(String value) {
        this.insurance = value;
    }

    /**
     * Gets the value of the memberName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     * Sets the value of the memberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberName(String value) {
        this.memberName = value;
    }

    /**
     * Gets the value of the installments property.
     * 
     */
    public int getInstallments() {
        return installments;
    }

    /**
     * Sets the value of the installments property.
     * 
     */
    public void setInstallments(int value) {
        this.installments = value;
    }

    /**
     * Gets the value of the modeOfDisbursement property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModeOfDisbursement() {
        return modeOfDisbursement;
    }

    /**
     * Sets the value of the modeOfDisbursement property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModeOfDisbursement(String value) {
        this.modeOfDisbursement = value;
    }

    /**
     * Gets the value of the installementPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInstallementPeriod() {
        return installementPeriod;
    }

    /**
     * Sets the value of the installementPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInstallementPeriod(String value) {
        this.installementPeriod = value;
    }

    /**
     * Gets the value of the repayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepayment() {
        return repayment;
    }

    /**
     * Sets the value of the repayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepayment(String value) {
        this.repayment = value;
    }

    /**
     * Gets the value of the loanProductTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanProductTypeName() {
        return loanProductTypeName;
    }

    /**
     * Sets the value of the loanProductTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanProductTypeName(String value) {
        this.loanProductTypeName = value;
    }

    /**
     * Gets the value of the repaymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepaymentMethod() {
        return repaymentMethod;
    }

    /**
     * Sets the value of the repaymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepaymentMethod(String value) {
        this.repaymentMethod = value;
    }

    /**
     * Gets the value of the monthlyRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyRepayment() {
        return monthlyRepayment;
    }

    /**
     * Sets the value of the monthlyRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyRepayment(String value) {
        this.monthlyRepayment = value;
    }

    /**
     * Gets the value of the monthlyInterest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMonthlyInterest() {
        return monthlyInterest;
    }

    /**
     * Sets the value of the monthlyInterest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMonthlyInterest(String value) {
        this.monthlyInterest = value;
    }

    /**
     * Gets the value of the smsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmsDescription() {
        return smsDescription;
    }

    /**
     * Sets the value of the smsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmsDescription(String value) {
        this.smsDescription = value;
    }

    /**
     * Gets the value of the eloan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEloan() {
        return eloan;
    }

    /**
     * Sets the value of the eloan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEloan(String value) {
        this.eloan = value;
    }

    /**
     * Gets the value of the payrollNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPayrollNo() {
        return payrollNo;
    }

    /**
     * Sets the value of the payrollNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPayrollNo(String value) {
        this.payrollNo = value;
    }

    /**
     * Gets the value of the loansPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoansPurpose() {
        return loansPurpose;
    }

    /**
     * Sets the value of the loansPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoansPurpose(String value) {
        this.loansPurpose = value;
    }

}
