
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanProductTypes complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanProductTypes">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestRate" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestCalculationMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InsurancePercentage" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RepayPeriods" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MinLoanAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MaxLoanAmount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanInterestAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RepaymentMethod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Active" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InterestCalculationType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Eloan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LoanRepaymentType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BackofficeFrontoffice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DisbursementAccount" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanProductTypes", namespace = "urn:microsoft-dynamics-nav/xmlports/x50034", propOrder = {
    "code",
    "description",
    "interestRate",
    "interestCalculationMethod",
    "insurancePercentage",
    "repayPeriods",
    "minLoanAmount",
    "maxLoanAmount",
    "loanAccount",
    "loanInterestAccount",
    "repaymentMethod",
    "active",
    "interestCalculationType",
    "eloan",
    "loanRepaymentType",
    "backofficeFrontoffice",
    "disbursementAccount"
})
public class LoanProductTypes {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "InterestRate", required = true)
    protected String interestRate;
    @XmlElement(name = "InterestCalculationMethod", required = true)
    protected String interestCalculationMethod;
    @XmlElement(name = "InsurancePercentage", required = true)
    protected String insurancePercentage;
    @XmlElement(name = "RepayPeriods", required = true)
    protected String repayPeriods;
    @XmlElement(name = "MinLoanAmount", required = true)
    protected String minLoanAmount;
    @XmlElement(name = "MaxLoanAmount", required = true)
    protected String maxLoanAmount;
    @XmlElement(name = "LoanAccount", required = true)
    protected String loanAccount;
    @XmlElement(name = "LoanInterestAccount", required = true)
    protected String loanInterestAccount;
    @XmlElement(name = "RepaymentMethod", required = true)
    protected String repaymentMethod;
    @XmlElement(name = "Active", required = true)
    protected String active;
    @XmlElement(name = "InterestCalculationType", required = true)
    protected String interestCalculationType;
    @XmlElement(name = "Eloan", required = true)
    protected String eloan;
    @XmlElement(name = "LoanRepaymentType", required = true)
    protected String loanRepaymentType;
    @XmlElement(name = "BackofficeFrontoffice", required = true)
    protected String backofficeFrontoffice;
    @XmlElement(name = "DisbursementAccount", required = true)
    protected String disbursementAccount;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the interestRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestRate() {
        return interestRate;
    }

    /**
     * Sets the value of the interestRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestRate(String value) {
        this.interestRate = value;
    }

    /**
     * Gets the value of the interestCalculationMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestCalculationMethod() {
        return interestCalculationMethod;
    }

    /**
     * Sets the value of the interestCalculationMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestCalculationMethod(String value) {
        this.interestCalculationMethod = value;
    }

    /**
     * Gets the value of the insurancePercentage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsurancePercentage() {
        return insurancePercentage;
    }

    /**
     * Sets the value of the insurancePercentage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsurancePercentage(String value) {
        this.insurancePercentage = value;
    }

    /**
     * Gets the value of the repayPeriods property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepayPeriods() {
        return repayPeriods;
    }

    /**
     * Sets the value of the repayPeriods property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepayPeriods(String value) {
        this.repayPeriods = value;
    }

    /**
     * Gets the value of the minLoanAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinLoanAmount() {
        return minLoanAmount;
    }

    /**
     * Sets the value of the minLoanAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinLoanAmount(String value) {
        this.minLoanAmount = value;
    }

    /**
     * Gets the value of the maxLoanAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxLoanAmount() {
        return maxLoanAmount;
    }

    /**
     * Sets the value of the maxLoanAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxLoanAmount(String value) {
        this.maxLoanAmount = value;
    }

    /**
     * Gets the value of the loanAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanAccount() {
        return loanAccount;
    }

    /**
     * Sets the value of the loanAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanAccount(String value) {
        this.loanAccount = value;
    }

    /**
     * Gets the value of the loanInterestAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanInterestAccount() {
        return loanInterestAccount;
    }

    /**
     * Sets the value of the loanInterestAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanInterestAccount(String value) {
        this.loanInterestAccount = value;
    }

    /**
     * Gets the value of the repaymentMethod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepaymentMethod() {
        return repaymentMethod;
    }

    /**
     * Sets the value of the repaymentMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepaymentMethod(String value) {
        this.repaymentMethod = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActive() {
        return active;
    }

    /**
     * Sets the value of the active property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActive(String value) {
        this.active = value;
    }

    /**
     * Gets the value of the interestCalculationType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInterestCalculationType() {
        return interestCalculationType;
    }

    /**
     * Sets the value of the interestCalculationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInterestCalculationType(String value) {
        this.interestCalculationType = value;
    }

    /**
     * Gets the value of the eloan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEloan() {
        return eloan;
    }

    /**
     * Sets the value of the eloan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEloan(String value) {
        this.eloan = value;
    }

    /**
     * Gets the value of the loanRepaymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanRepaymentType() {
        return loanRepaymentType;
    }

    /**
     * Sets the value of the loanRepaymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanRepaymentType(String value) {
        this.loanRepaymentType = value;
    }

    /**
     * Gets the value of the backofficeFrontoffice property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBackofficeFrontoffice() {
        return backofficeFrontoffice;
    }

    /**
     * Sets the value of the backofficeFrontoffice property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBackofficeFrontoffice(String value) {
        this.backofficeFrontoffice = value;
    }

    /**
     * Gets the value of the disbursementAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDisbursementAccount() {
        return disbursementAccount;
    }

    /**
     * Sets the value of the disbursementAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDisbursementAccount(String value) {
        this.disbursementAccount = value;
    }

}
