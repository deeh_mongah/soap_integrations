
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MembersCompany complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MembersCompany">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Totalnoofclients" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CurrentLoan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NoofFemaleClients" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NoofMaleClients" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="LoanProductTypeFilter" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MembersCompany", namespace = "urn:microsoft-dynamics-nav/xmlports/x50039", propOrder = {
    "code",
    "description",
    "totalnoofclients",
    "currentLoan",
    "noofFemaleClients",
    "noofMaleClients",
    "loanProductTypeFilter"
})
public class MembersCompany {

    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "Totalnoofclients", defaultValue = "0")
    protected int totalnoofclients;
    @XmlElement(name = "CurrentLoan", required = true)
    protected String currentLoan;
    @XmlElement(name = "NoofFemaleClients", defaultValue = "0")
    protected int noofFemaleClients;
    @XmlElement(name = "NoofMaleClients", defaultValue = "0")
    protected int noofMaleClients;
    @XmlElement(name = "LoanProductTypeFilter", required = true)
    protected String loanProductTypeFilter;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the totalnoofclients property.
     * 
     */
    public int getTotalnoofclients() {
        return totalnoofclients;
    }

    /**
     * Sets the value of the totalnoofclients property.
     * 
     */
    public void setTotalnoofclients(int value) {
        this.totalnoofclients = value;
    }

    /**
     * Gets the value of the currentLoan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentLoan() {
        return currentLoan;
    }

    /**
     * Sets the value of the currentLoan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentLoan(String value) {
        this.currentLoan = value;
    }

    /**
     * Gets the value of the noofFemaleClients property.
     * 
     */
    public int getNoofFemaleClients() {
        return noofFemaleClients;
    }

    /**
     * Sets the value of the noofFemaleClients property.
     * 
     */
    public void setNoofFemaleClients(int value) {
        this.noofFemaleClients = value;
    }

    /**
     * Gets the value of the noofMaleClients property.
     * 
     */
    public int getNoofMaleClients() {
        return noofMaleClients;
    }

    /**
     * Sets the value of the noofMaleClients property.
     * 
     */
    public void setNoofMaleClients(int value) {
        this.noofMaleClients = value;
    }

    /**
     * Gets the value of the loanProductTypeFilter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoanProductTypeFilter() {
        return loanProductTypeFilter;
    }

    /**
     * Sets the value of the loanProductTypeFilter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoanProductTypeFilter(String value) {
        this.loanProductTypeFilter = value;
    }

}
