
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exportMembersCompany" type="{urn:microsoft-dynamics-nav/xmlports/x50039}MemberscompanyRoot"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exportMembersCompany"
})
@XmlRootElement(name = "GetMembercompany")
public class GetMembercompany {

    @XmlElement(required = true)
    protected MemberscompanyRoot exportMembersCompany;

    /**
     * Gets the value of the exportMembersCompany property.
     * 
     * @return
     *     possible object is
     *     {@link MemberscompanyRoot }
     *     
     */
    public MemberscompanyRoot getExportMembersCompany() {
        return exportMembersCompany;
    }

    /**
     * Sets the value of the exportMembersCompany property.
     * 
     * @param value
     *     allowed object is
     *     {@link MemberscompanyRoot }
     *     
     */
    public void setExportMembersCompany(MemberscompanyRoot value) {
        this.exportMembersCompany = value;
    }

}
