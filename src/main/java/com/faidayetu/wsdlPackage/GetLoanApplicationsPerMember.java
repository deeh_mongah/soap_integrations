
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exportLoanApplications" type="{urn:microsoft-dynamics-nav/xmlports/x50035}LoanApplicationsRoot"/>
 *         &lt;element name="memberCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exportLoanApplications",
    "memberCode"
})
@XmlRootElement(name = "GetLoanApplicationsPerMember")
public class GetLoanApplicationsPerMember {

    @XmlElement(required = true)
    protected LoanApplicationsRoot exportLoanApplications;
    @XmlElement(required = true)
    protected String memberCode;

    /**
     * Gets the value of the exportLoanApplications property.
     * 
     * @return
     *     possible object is
     *     {@link LoanApplicationsRoot }
     *     
     */
    public LoanApplicationsRoot getExportLoanApplications() {
        return exportLoanApplications;
    }

    /**
     * Sets the value of the exportLoanApplications property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoanApplicationsRoot }
     *     
     */
    public void setExportLoanApplications(LoanApplicationsRoot value) {
        this.exportLoanApplications = value;
    }

    /**
     * Gets the value of the memberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberCode() {
        return memberCode;
    }

    /**
     * Sets the value of the memberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberCode(String value) {
        this.memberCode = value;
    }

}
