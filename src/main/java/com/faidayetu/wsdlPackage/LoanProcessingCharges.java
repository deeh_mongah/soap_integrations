
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoanProcessingCharges complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoanProcessingCharges">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Loantype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Description" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CalculationMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AccountNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="LedgerFees" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="InsuranceCharges" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MinimumFees" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoanProcessingCharges", namespace = "urn:microsoft-dynamics-nav/xmlports/x50036", propOrder = {
    "loantype",
    "code",
    "description",
    "calculationMode",
    "value",
    "accountType",
    "accountNo",
    "ledgerFees",
    "insuranceCharges",
    "minimumFees"
})
public class LoanProcessingCharges {

    @XmlElement(name = "Loantype", required = true)
    protected String loantype;
    @XmlElement(name = "Code", required = true)
    protected String code;
    @XmlElement(name = "Description", required = true)
    protected String description;
    @XmlElement(name = "CalculationMode", required = true)
    protected String calculationMode;
    @XmlElement(name = "Value", required = true)
    protected String value;
    @XmlElement(name = "AccountType", required = true)
    protected String accountType;
    @XmlElement(name = "AccountNo", required = true)
    protected String accountNo;
    @XmlElement(name = "LedgerFees", required = true)
    protected String ledgerFees;
    @XmlElement(name = "InsuranceCharges", required = true)
    protected String insuranceCharges;
    @XmlElement(name = "MinimumFees", required = true)
    protected String minimumFees;

    /**
     * Gets the value of the loantype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLoantype() {
        return loantype;
    }

    /**
     * Sets the value of the loantype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLoantype(String value) {
        this.loantype = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the calculationMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculationMode() {
        return calculationMode;
    }

    /**
     * Sets the value of the calculationMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculationMode(String value) {
        this.calculationMode = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the accountNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNo() {
        return accountNo;
    }

    /**
     * Sets the value of the accountNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNo(String value) {
        this.accountNo = value;
    }

    /**
     * Gets the value of the ledgerFees property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLedgerFees() {
        return ledgerFees;
    }

    /**
     * Sets the value of the ledgerFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLedgerFees(String value) {
        this.ledgerFees = value;
    }

    /**
     * Gets the value of the insuranceCharges property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInsuranceCharges() {
        return insuranceCharges;
    }

    /**
     * Sets the value of the insuranceCharges property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInsuranceCharges(String value) {
        this.insuranceCharges = value;
    }

    /**
     * Gets the value of the minimumFees property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinimumFees() {
        return minimumFees;
    }

    /**
     * Sets the value of the minimumFees property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinimumFees(String value) {
        this.minimumFees = value;
    }

}
