
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="exportLoanTypes" type="{urn:microsoft-dynamics-nav/xmlports/x50034}Root"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "exportLoanTypes"
})
@XmlRootElement(name = "GetLoanProducts_Result")
public class GetLoanProductsResult {

    @XmlElement(required = true)
    protected Root exportLoanTypes;

    /**
     * Gets the value of the exportLoanTypes property.
     * 
     * @return
     *     possible object is
     *     {@link Root }
     *     
     */
    public Root getExportLoanTypes() {
        return exportLoanTypes;
    }

    /**
     * Sets the value of the exportLoanTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link Root }
     *     
     */
    public void setExportLoanTypes(Root value) {
        this.exportLoanTypes = value;
    }

}
