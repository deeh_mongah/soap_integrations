
package com.faidayetu.wsdlPackage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Member complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Member">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MemberNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MemberName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IDno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MobilePhoneNo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Email" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DateofBirth" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CompanyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Member", namespace = "urn:microsoft-dynamics-nav/xmlports/x50038", propOrder = {
    "memberNo",
    "memberName",
    "iDno",
    "mobilePhoneNo",
    "email",
    "dateofBirth",
    "companyCode",
    "companyName"
})
public class Member {

    @XmlElement(name = "MemberNo", required = true)
    protected String memberNo;
    @XmlElement(name = "MemberName", required = true)
    protected String memberName;
    @XmlElement(name = "IDno", required = true)
    protected String iDno;
    @XmlElement(name = "MobilePhoneNo", required = true)
    protected String mobilePhoneNo;
    @XmlElement(name = "Email", required = true)
    protected String email;
    @XmlElement(name = "DateofBirth", required = true)
    protected String dateofBirth;
    @XmlElement(name = "CompanyCode", required = true)
    protected String companyCode;
    @XmlElement(name = "CompanyName", required = true)
    protected String companyName;

    /**
     * Gets the value of the memberNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberNo() {
        return memberNo;
    }

    /**
     * Sets the value of the memberNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberNo(String value) {
        this.memberNo = value;
    }

    /**
     * Gets the value of the memberName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMemberName() {
        return memberName;
    }

    /**
     * Sets the value of the memberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMemberName(String value) {
        this.memberName = value;
    }

    /**
     * Gets the value of the iDno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDno() {
        return iDno;
    }

    /**
     * Sets the value of the iDno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDno(String value) {
        this.iDno = value;
    }

    /**
     * Gets the value of the mobilePhoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilePhoneNo() {
        return mobilePhoneNo;
    }

    /**
     * Sets the value of the mobilePhoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilePhoneNo(String value) {
        this.mobilePhoneNo = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the dateofBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateofBirth() {
        return dateofBirth;
    }

    /**
     * Sets the value of the dateofBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateofBirth(String value) {
        this.dateofBirth = value;
    }

    /**
     * Gets the value of the companyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the value of the companyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyCode(String value) {
        this.companyCode = value;
    }

    /**
     * Gets the value of the companyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyName() {
        return companyName;
    }

    /**
     * Sets the value of the companyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyName(String value) {
        this.companyName = value;
    }

}
