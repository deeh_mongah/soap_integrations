
package com.faidayetu.wsdlPackage;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pMemberCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pApplicationDate" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="pLoanProductType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pCompanyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pRequestedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pApprovedAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pInterest" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pInsurance" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pMemberName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pInstallments" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pModeofDisbursement" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pInstalmentPeriod" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pRepayment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pLoanProductTypeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pRepaymentMethod" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="pMonthlyRepayment" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pMonthlyInterest" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="pSMSDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pEloan" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="pPayrollno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pLoanPurpose" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "pMemberCode",
    "pApplicationDate",
    "pLoanProductType",
    "pCompanyCode",
    "pRequestedAmount",
    "pApprovedAmount",
    "pInterest",
    "pInsurance",
    "pMemberName",
    "pInstallments",
    "pModeofDisbursement",
    "pInstalmentPeriod",
    "pRepayment",
    "pLoanProductTypeName",
    "pRepaymentMethod",
    "pMonthlyRepayment",
    "pMonthlyInterest",
    "psmsDescription",
    "pEloan",
    "pPayrollno",
    "pLoanPurpose"
})
@XmlRootElement(name = "CreateLoanApplication")
public class CreateLoanApplication {

    @XmlElement(required = true)
    protected String pLoanNo;
    @XmlElement(required = true)
    protected String pMemberCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar pApplicationDate;
    @XmlElement(required = true)
    protected String pLoanProductType;
    @XmlElement(required = true)
    protected String pCompanyCode;
    @XmlElement(required = true)
    protected BigDecimal pRequestedAmount;
    @XmlElement(required = true)
    protected BigDecimal pApprovedAmount;
    @XmlElement(required = true)
    protected BigDecimal pInterest;
    @XmlElement(required = true)
    protected BigDecimal pInsurance;
    @XmlElement(required = true)
    protected String pMemberName;
    protected int pInstallments;
    protected int pModeofDisbursement;
    @XmlElement(required = true)
    protected String pInstalmentPeriod;
    @XmlElement(required = true)
    protected BigDecimal pRepayment;
    @XmlElement(required = true)
    protected String pLoanProductTypeName;
    protected int pRepaymentMethod;
    @XmlElement(required = true)
    protected BigDecimal pMonthlyRepayment;
    @XmlElement(required = true)
    protected BigDecimal pMonthlyInterest;
    @XmlElement(name = "pSMSDescription", required = true)
    protected String psmsDescription;
    protected boolean pEloan;
    @XmlElement(required = true)
    protected String pPayrollno;
    @XmlElement(required = true)
    protected String pLoanPurpose;

    public String getpLoanNo() {
        return pLoanNo;
    }

    public void setpLoanNo(String pLoanNo) {
        this.pLoanNo = pLoanNo;
    }

    /**
     * Gets the value of the pMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getpMemberCode() {
        return pMemberCode;
    }

    /**
     * Sets the value of the pMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setpMemberCode(String value) {
        this.pMemberCode = value;
    }

    /**
     * Gets the value of the pApplicationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPApplicationDate() {
        return pApplicationDate;
    }

    /**
     * Sets the value of the pApplicationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPApplicationDate(XMLGregorianCalendar value) {
        this.pApplicationDate = value;
    }

    /**
     * Gets the value of the pLoanProductType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLoanProductType() {
        return pLoanProductType;
    }

    /**
     * Sets the value of the pLoanProductType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLoanProductType(String value) {
        this.pLoanProductType = value;
    }

    /**
     * Gets the value of the pCompanyCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPCompanyCode() {
        return pCompanyCode;
    }

    /**
     * Sets the value of the pCompanyCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPCompanyCode(String value) {
        this.pCompanyCode = value;
    }

    /**
     * Gets the value of the pRequestedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPRequestedAmount() {
        return pRequestedAmount;
    }

    /**
     * Sets the value of the pRequestedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPRequestedAmount(BigDecimal value) {
        this.pRequestedAmount = value;
    }

    /**
     * Gets the value of the pApprovedAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPApprovedAmount() {
        return pApprovedAmount;
    }

    /**
     * Sets the value of the pApprovedAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPApprovedAmount(BigDecimal value) {
        this.pApprovedAmount = value;
    }

    /**
     * Gets the value of the pInterest property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPInterest() {
        return pInterest;
    }

    /**
     * Sets the value of the pInterest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPInterest(BigDecimal value) {
        this.pInterest = value;
    }

    /**
     * Gets the value of the pInsurance property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPInsurance() {
        return pInsurance;
    }

    /**
     * Sets the value of the pInsurance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPInsurance(BigDecimal value) {
        this.pInsurance = value;
    }

    /**
     * Gets the value of the pMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPMemberName() {
        return pMemberName;
    }

    /**
     * Sets the value of the pMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPMemberName(String value) {
        this.pMemberName = value;
    }

    /**
     * Gets the value of the pInstallments property.
     * 
     */
    public int getPInstallments() {
        return pInstallments;
    }

    /**
     * Sets the value of the pInstallments property.
     * 
     */
    public void setPInstallments(int value) {
        this.pInstallments = value;
    }

    /**
     * Gets the value of the pModeofDisbursement property.
     * 
     */
    public int getPModeofDisbursement() {
        return pModeofDisbursement;
    }

    /**
     * Sets the value of the pModeofDisbursement property.
     * 
     */
    public void setPModeofDisbursement(int value) {
        this.pModeofDisbursement = value;
    }

    /**
     * Gets the value of the pInstalmentPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPInstalmentPeriod() {
        return pInstalmentPeriod;
    }

    /**
     * Sets the value of the pInstalmentPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPInstalmentPeriod(String value) {
        this.pInstalmentPeriod = value;
    }

    /**
     * Gets the value of the pRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPRepayment() {
        return pRepayment;
    }

    /**
     * Sets the value of the pRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPRepayment(BigDecimal value) {
        this.pRepayment = value;
    }

    /**
     * Gets the value of the pLoanProductTypeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLoanProductTypeName() {
        return pLoanProductTypeName;
    }

    /**
     * Sets the value of the pLoanProductTypeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLoanProductTypeName(String value) {
        this.pLoanProductTypeName = value;
    }

    /**
     * Gets the value of the pRepaymentMethod property.
     * 
     */
    public int getPRepaymentMethod() {
        return pRepaymentMethod;
    }

    /**
     * Sets the value of the pRepaymentMethod property.
     * 
     */
    public void setPRepaymentMethod(int value) {
        this.pRepaymentMethod = value;
    }

    /**
     * Gets the value of the pMonthlyRepayment property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPMonthlyRepayment() {
        return pMonthlyRepayment;
    }

    /**
     * Sets the value of the pMonthlyRepayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPMonthlyRepayment(BigDecimal value) {
        this.pMonthlyRepayment = value;
    }

    /**
     * Gets the value of the pMonthlyInterest property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPMonthlyInterest() {
        return pMonthlyInterest;
    }

    /**
     * Sets the value of the pMonthlyInterest property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPMonthlyInterest(BigDecimal value) {
        this.pMonthlyInterest = value;
    }

    /**
     * Gets the value of the psmsDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPSMSDescription() {
        return psmsDescription;
    }

    /**
     * Sets the value of the psmsDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPSMSDescription(String value) {
        this.psmsDescription = value;
    }

    /**
     * Gets the value of the pEloan property.
     * 
     */
    public boolean isPEloan() {
        return pEloan;
    }

    /**
     * Sets the value of the pEloan property.
     * 
     */
    public void setPEloan(boolean value) {
        this.pEloan = value;
    }

    /**
     * Gets the value of the pPayrollno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPPayrollno() {
        return pPayrollno;
    }

    /**
     * Sets the value of the pPayrollno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPPayrollno(String value) {
        this.pPayrollno = value;
    }

    /**
     * Gets the value of the pLoanPurpose property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPLoanPurpose() {
        return pLoanPurpose;
    }

    /**
     * Sets the value of the pLoanPurpose property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPLoanPurpose(String value) {
        this.pLoanPurpose = value;
    }

}
