package com.faidayetu.webServiceApis;

import com.faidayetu.Service.MemberDetailsService;
import com.faidayetu.ServiceInterface.MemberServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import vm.MemberDetailsRequest;

import javax.validation.Valid;

@RequestMapping("/api/member")
@RestController
public class MemberController {

   @Autowired
    MemberServiceInterface memberServiceInterface;

    @PostMapping("/getmember")
    public Object getMemberDetailsResult(@RequestBody @Valid MemberDetailsRequest request) {
        return memberServiceInterface.retriveCustomer(request);
    }

}
