package com.faidayetu.webServiceApis;

import com.faidayetu.Service.LoanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import vm.*;

import javax.validation.Valid;
import javax.xml.bind.JAXBException;
import javax.xml.datatype.DatatypeConfigurationException;

@RequestMapping("/api/loan")
@RestController
public class LoanController {

    @Autowired
    LoanService loanService;

    @PostMapping("/get-loan/{Loanno}")
    public Object getLoan(@PathVariable String Loanno){
        return loanService.getLoanApplications(Loanno);
    }

    @PostMapping("/get-loan-per-member")
    public Object getLoanPerMember(@RequestBody @Valid MemberDetailsRequest request){
        return loanService.getLoanApplicationsPerMember(request);
    }
    @PostMapping("get-loan-charges")
    public Object getLoanCharges(@RequestBody LoanCharges request) throws JAXBException {
        return loanService.getLoanChargesResult(request);
    }
    @PostMapping("get-loan-Product")
    public Object getLoanProducts(@RequestBody LoanProducts request){
        return loanService.getLoanProductsResult(request);
    }
    @PostMapping("get-loan-processing-charges")
    public Object getLoanProcessingCharges(@RequestBody LoanProccessingCharges request){
        return loanService.getLoanProcessingChargesResult(request);
    }
    @PostMapping("/create-loan-application")
    public Object createLoanApplication(@RequestBody @Valid LoanApplication request) throws DatatypeConfigurationException {
        return loanService.createLoanApplicationResult(request);
    }
    @PostMapping("/modify-loan-application")
    public Object modifyLoanApplication(@RequestBody @Valid ModifyLoanApplication request) throws DatatypeConfigurationException {
        return loanService.modifyLoanApplication(request);
    }
}
