package com.faidayetu.Service;


import com.faidayetu.wsdlPackage.*;
import com.faidayetu.wsdlPackage.LoanCharges;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import vm.*;
import vm.ModifyLoanApplication;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


@Service
public class LoanService {

    @Value("${faida.endpoint}")
    private String serviceEndPiont;

    @Autowired WebServiceTemplate webServiceTemplate;

   private ObjectFactory objectFactory;




    public GetLoanApplicationsResult getLoanApplications(String Loanno){
        LoanApplicationsRoot loanApplicationsRoot = new LoanApplicationsRoot();

        GetLoanApplications getLoanApplication = new GetLoanApplications();
        getLoanApplication.setPLoanno(Loanno);
        getLoanApplication.setExportLoanApplications(loanApplicationsRoot);

        GetLoanApplicationsResult result = (GetLoanApplicationsResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getLoanApplication,new SoapActionCallback(""));
        return result;

    }
    public GetLoanApplicationsPerMemberResult getLoanApplicationsPerMember(MemberDetailsRequest request){
        LoanApplicationsRoot loanApplicationsRoot = new LoanApplicationsRoot();

        GetLoanApplicationsPerMember getLoanApplicationsPerMember = new GetLoanApplicationsPerMember();
        getLoanApplicationsPerMember.setExportLoanApplications(loanApplicationsRoot);
        getLoanApplicationsPerMember.setMemberCode(request.getMemberCode());

        GetLoanApplicationsPerMemberResult getLoanApplicationsPerMemberResult = (GetLoanApplicationsPerMemberResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getLoanApplicationsPerMember,new SoapActionCallback(""));
        return getLoanApplicationsPerMemberResult;
    }
    public GetLoanChargesResult getLoanChargesResult(vm.LoanCharges request) {
        com.faidayetu.wsdlPackage.LoanCharges loanCharges =  new com.faidayetu.wsdlPackage.LoanCharges();
        loanCharges.setCode(request.getCode());
        loanCharges.setDescription(request.getDescription());

        LoanChargesCodesRoot loanChargesCodesRoot = new LoanChargesCodesRoot();

        GetLoanCharges getLoanCharges = new GetLoanCharges();
        getLoanCharges.setExportLoanProcessingcharges(loanChargesCodesRoot);

        GetLoanChargesResult getLoanChargesResult = (GetLoanChargesResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getLoanCharges,new SoapActionCallback(""));
        return getLoanChargesResult;
    }

    public GetLoanProductsResult getLoanProductsResult(LoanProducts request){
        LoanProductTypes loanProductTypes = new LoanProductTypes();

        loanProductTypes.setCode(request.getCode());
        loanProductTypes.setDescription(request.getDescription());
        loanProductTypes.setInterestRate(request.getInterestRate());
        loanProductTypes.setBackofficeFrontoffice(request.getBackOfficeFrontOffice());
        loanProductTypes.setInterestCalculationMethod(request.getInterestCalculationMode());
        loanProductTypes.setInsurancePercentage(request.getInsurancePercentage());
        loanProductTypes.setRepayPeriods(request.getRepayPeriod());
        loanProductTypes.setMaxLoanAmount(request.getMaxLoanAmount());
        loanProductTypes.setMinLoanAmount(request.getMinLoanAmount());
        loanProductTypes.setLoanAccount(request.getLoanAccount());
        loanProductTypes.setLoanAccount(request.getLoanInterestAccount());
        loanProductTypes.setRepaymentMethod(request.getRepaymentMethod());
        loanProductTypes.setActive(request.getActive());
        loanProductTypes.setInterestCalculationType(request.getInterestCalculationType());
        loanProductTypes.setEloan(request.geteLoan());
        loanProductTypes.setLoanRepaymentType(request.getLoanRepaymentType());
        loanProductTypes.setBackofficeFrontoffice(request.getBackOfficeFrontOffice());
        loanProductTypes.setDisbursementAccount(request.getDibursementAccount());

        Root root = new Root();

        GetLoanProducts getLoanProducts = new GetLoanProducts();
        getLoanProducts.setExportLoanTypes(root);

        GetLoanProductsResult getLoanProductsResult = (GetLoanProductsResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getLoanProducts,new SoapActionCallback(""));

        return getLoanProductsResult;

    }
    public GetLoanProcessingChargesResult getLoanProcessingChargesResult(LoanProccessingCharges request){
        LoanProcessingCharges loanProcessingCharges = new LoanProcessingCharges();

        loanProcessingCharges.setAccountNo(request.getAccountNo());
        loanProcessingCharges.setAccountType(request.getAccountType());
        loanProcessingCharges.setCalculationMode(request.getCalculationMode());
        loanProcessingCharges.setCode(request.getCode());
        loanProcessingCharges.setDescription(request.getDescription());
        loanProcessingCharges.setValue(request.getValue());
        loanProcessingCharges.setLedgerFees(request.getLedgeFees());
        loanProcessingCharges.setMinimumFees(request.getMinimumFees());
        loanProcessingCharges.setLoantype(request.getLoanType());
        loanProcessingCharges.setInsuranceCharges(request.getInsuranceCharges());

        LoanChargesRoot loanChargesRoot = new LoanChargesRoot();

        GetLoanProcessingCharges getLoanProcessingCharges = new GetLoanProcessingCharges();
        getLoanProcessingCharges.setExportLoanProcessingcharges(loanChargesRoot);

        GetLoanProcessingChargesResult getLoanProcessingChargesResult = (GetLoanProcessingChargesResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getLoanProcessingCharges,new SoapActionCallback(""));
        return getLoanProcessingChargesResult;
    }

    public Object createLoanApplicationResult(LoanApplication request) throws DatatypeConfigurationException {


        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");


        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(request.getApplicationDate());
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1,calendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);

        CreateLoanApplication createLoanApplication = new CreateLoanApplication();

        createLoanApplication.setpLoanNo(request.getpLoanNo());
        createLoanApplication.setpMemberCode(request.getpMemberCode());
        createLoanApplication.setPApplicationDate(xmlGregorianCalendar);
        createLoanApplication.setPLoanProductType(request.getLoanProductType());
        createLoanApplication.setPCompanyCode(request.getCompanyCode());
        createLoanApplication.setPRequestedAmount(request.getRequestedAmount());
        createLoanApplication.setPApprovedAmount(request.getApprovedAmount());
        createLoanApplication.setPInterest(request.getInterest());
        createLoanApplication.setPInsurance(request.getInsurance());
        createLoanApplication.setPMemberName(request.getMemberName());
        createLoanApplication.setPInstallments(request.getInstallments());
        createLoanApplication.setPModeofDisbursement(request.getModeOfDisbursement());
        createLoanApplication.setPInstalmentPeriod(request.getInstallementPeriod());
        createLoanApplication.setPRepayment(request.getRepayment());
        createLoanApplication.setPLoanProductTypeName(request.getLoanProductTypeName());
        createLoanApplication.setPRepaymentMethod(request.getRepaymentMethod());
        createLoanApplication.setPMonthlyRepayment(request.getMonthlyRepayment());
        createLoanApplication.setPMonthlyInterest(request.getMonthlyInterest());
        createLoanApplication.setPSMSDescription(request.getSmsDescription());
        createLoanApplication.setPEloan(request.isEloan());
        createLoanApplication.setPPayrollno(request.getPayrollNo());
        createLoanApplication.setPLoanPurpose(request.getLoansPurpose());

        System.err.println(createLoanApplication.getpMemberCode());
        System.err.println(createLoanApplication);
        CreateLoanApplicationResult createLoanApplicationResult = (CreateLoanApplicationResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,createLoanApplication,new SoapActionCallback(""));

        return createLoanApplicationResult;

    }
    public Object modifyLoanApplication(ModifyLoanApplication request) throws DatatypeConfigurationException {
        com.faidayetu.wsdlPackage.ModifyLoanApplication modifyLoanApplication = new com.faidayetu.wsdlPackage.ModifyLoanApplication();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(request.getApplicationDate());
        XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH)+1,calendar.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);


        modifyLoanApplication.setPLoanNo(request.getLoanNo());
        modifyLoanApplication.setPMemberCode(request.getMemberCode());
        modifyLoanApplication.setPApplicationDate(xmlGregorianCalendar);
        modifyLoanApplication.setPLoanProductType(request.getLoanProductType());
        modifyLoanApplication.setPCompanyCode(request.getCompanyCode());
        modifyLoanApplication.setPRequestedAmount(request.getRequestedAmount());
        modifyLoanApplication.setPApprovedAmount(request.getApprovedAmount());
        modifyLoanApplication.setPInterest(request.getInterest());
        modifyLoanApplication.setPInsurance(request.getInsurance());
        modifyLoanApplication.setPMemberName(request.getMemberName());
        modifyLoanApplication.setPInstallments(request.getInstallments());
        modifyLoanApplication.setPModeofDisbursement(request.getModeofDisbursement());
        modifyLoanApplication.setPInstalmentPeriod(request.getInstalmentPeriod());
        modifyLoanApplication.setPRepayment(request.getRepayment());
        modifyLoanApplication.setPLoanProductTypeName(request.getLoanProductTypeName());
        modifyLoanApplication.setPRepaymentMethod(request.getRepaymentMethod());
        modifyLoanApplication.setPMonthlyRepayment(request.getMonthlyRepayment());
        modifyLoanApplication.setPMonthlyInterest(request.getMonthlyInterest());
        modifyLoanApplication.setPSMSDescription(request.getSmsDescription());
        modifyLoanApplication.setPEloan(request.ispEloan());
        modifyLoanApplication.setPPayrollno(request.getPayrollno());
        modifyLoanApplication.setPLoanPurpose(request.getLoanPurpose());


        ModifyLoanApplicationResult modifyLoanApplicationResult = (ModifyLoanApplicationResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,modifyLoanApplication,new SoapActionCallback(""));

        return modifyLoanApplicationResult;




    }




}
