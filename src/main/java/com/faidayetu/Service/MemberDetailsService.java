package com.faidayetu.Service;

import com.faidayetu.ServiceInterface.MemberServiceInterface;
import com.faidayetu.wsdlPackage.GetMemberDetails;
import com.faidayetu.wsdlPackage.GetMemberDetailsResult;
import com.faidayetu.wsdlPackage.Member;
import com.faidayetu.wsdlPackage.MemberDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;
import vm.MemberDetailsRequest;


@Service
public class MemberDetailsService   implements MemberServiceInterface {

    @Value("${faida.endpoint}")
    private String serviceEndPiont;

    @Autowired WebServiceTemplate webServiceTemplate;


    @Override
    public Object retriveCustomer(MemberDetailsRequest request) {
        MemberDetails memberDetails = new MemberDetails();


        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.setMemberCode(request.getMemberCode());
        getMemberDetails.setExportMemberDetails(memberDetails);

        getMemberDetails.getExportMemberDetails();

        GetMemberDetailsResult getMemberDetailsResult = (GetMemberDetailsResult) webServiceTemplate.marshalSendAndReceive(serviceEndPiont,getMemberDetails,new SoapActionCallback(""));
        return getMemberDetailsResult;
    }
}
