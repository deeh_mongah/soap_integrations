package com.faidayetu.ServiceInterface;

import com.faidayetu.wsdlPackage.GetMemberDetails;
import com.faidayetu.wsdlPackage.GetMemberDetailsResult;
import vm.MemberDetailsRequest;

public interface MemberServiceInterface {

    public Object retriveCustomer(MemberDetailsRequest request);
}
