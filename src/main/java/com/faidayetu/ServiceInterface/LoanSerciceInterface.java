package com.faidayetu.ServiceInterface;

import ResponseModel.ResponseModel;
import com.faidayetu.wsdlPackage.CreateLoanApplication;
import com.faidayetu.wsdlPackage.CreateLoanApplicationResult;
import vm.*;

public interface LoanSerciceInterface {

    public CreateLoanApplicationResult createLoanApplication(CreateLoanApplication request);

    public ResponseModel loanCharges(LoanCharges request);

    public ResponseModel loanProcessingCharges(LoanProccessingCharges request);

    public ResponseModel loanProduct(LoanProducts request);

    public ResponseModel loanPerMember(MemberDetailsRequest request);

}
