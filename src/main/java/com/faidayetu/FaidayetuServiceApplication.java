package com.faidayetu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class FaidayetuServiceApplication{

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication( FaidayetuServiceApplication.class );
		app.addListeners(new ApplicationPidFileWriter() );
		app.run( args );
	}

}
