package com.faidayetu.webServices;

import com.faidayetu.Service.MemberDetailsService;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.NTCredentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;
import org.springframework.ws.transport.WebServiceMessageSender;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

/**
 * Created by User on 3/5/2020.
 */

@Configuration
public class soapConfig {

    @Value("${faida.endpoint}")
    public String serviceEndPiont;

    @Value("${faida.Username}")
    public String Username;

    @Value("${faida.Password}")
    public String Password;

    @Value("${faida.Domain}")
    public String Domain;

    @Bean
    public HttpComponentsMessageSender httpComponentsMessageSender(){
        HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
        NTCredentials credentials = new NTCredentials(Username,String.valueOf(Password),null,Domain);
        httpComponentsMessageSender.setCredentials(credentials);
        return httpComponentsMessageSender;
    }
    @Bean
    public Jaxb2Marshaller marshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan("com.faidayetu.wsdlPackage");
        return marshaller;
    }
    @Bean
    public WebServiceTemplate webServiceTemplate(Jaxb2Marshaller marshaller, WebServiceMessageSender messageSender){
        WebServiceTemplate webServiceTemplate = new WebServiceTemplate(marshaller , marshaller);
        webServiceTemplate.setMessageSender(messageSender);
        return webServiceTemplate;
    }






}
