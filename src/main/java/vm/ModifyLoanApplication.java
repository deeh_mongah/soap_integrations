package vm;

import java.math.BigDecimal;
import java.util.Date;

public class ModifyLoanApplication {

    protected String LoanNo;

    protected String MemberCode;

    protected Date ApplicationDate;

    protected String LoanProductType;

    protected String CompanyCode;

    protected BigDecimal RequestedAmount;

    protected BigDecimal ApprovedAmount;

    protected BigDecimal Interest;

    protected BigDecimal Insurance;

    protected String MemberName;

    protected int Installments;

    protected int ModeofDisbursement;

    protected String InstalmentPeriod;

    protected BigDecimal Repayment;

    protected String LoanProductTypeName;

    protected int RepaymentMethod;

    protected BigDecimal MonthlyRepayment;

    protected BigDecimal MonthlyInterest;

    protected String smsDescription;

    protected boolean pEloan;

    protected String Payrollno;

    protected String LoanPurpose;

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public String getMemberCode() {
        return MemberCode;
    }

    public void setMemberCode(String memberCode) {
        MemberCode = memberCode;
    }

    public Date getApplicationDate() {
        return ApplicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        ApplicationDate = applicationDate;
    }

    public String getLoanProductType() {
        return LoanProductType;
    }

    public void setLoanProductType(String loanProductType) {
        LoanProductType = loanProductType;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }

    public BigDecimal getRequestedAmount() {
        return RequestedAmount;
    }

    public void setRequestedAmount(BigDecimal requestedAmount) {
        RequestedAmount = requestedAmount;
    }

    public BigDecimal getApprovedAmount() {
        return ApprovedAmount;
    }

    public void setApprovedAmount(BigDecimal approvedAmount) {
        ApprovedAmount = approvedAmount;
    }

    public BigDecimal getInterest() {
        return Interest;
    }

    public void setInterest(BigDecimal interest) {
        Interest = interest;
    }

    public BigDecimal getInsurance() {
        return Insurance;
    }

    public void setInsurance(BigDecimal insurance) {
        Insurance = insurance;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public int getInstallments() {
        return Installments;
    }

    public void setInstallments(int installments) {
        Installments = installments;
    }

    public int getModeofDisbursement() {
        return ModeofDisbursement;
    }

    public void setModeofDisbursement(int modeofDisbursement) {
        ModeofDisbursement = modeofDisbursement;
    }

    public String getInstalmentPeriod() {
        return InstalmentPeriod;
    }

    public void setInstalmentPeriod(String instalmentPeriod) {
        InstalmentPeriod = instalmentPeriod;
    }

    public BigDecimal getRepayment() {
        return Repayment;
    }

    public void setRepayment(BigDecimal repayment) {
        Repayment = repayment;
    }

    public String getLoanProductTypeName() {
        return LoanProductTypeName;
    }

    public void setLoanProductTypeName(String loanProductTypeName) {
        LoanProductTypeName = loanProductTypeName;
    }

    public int getRepaymentMethod() {
        return RepaymentMethod;
    }

    public void setRepaymentMethod(int repaymentMethod) {
        RepaymentMethod = repaymentMethod;
    }

    public BigDecimal getMonthlyRepayment() {
        return MonthlyRepayment;
    }

    public void setMonthlyRepayment(BigDecimal monthlyRepayment) {
        MonthlyRepayment = monthlyRepayment;
    }

    public BigDecimal getMonthlyInterest() {
        return MonthlyInterest;
    }

    public void setMonthlyInterest(BigDecimal monthlyInterest) {
        MonthlyInterest = monthlyInterest;
    }

    public String getSmsDescription() {
        return smsDescription;
    }

    public void setSmsDescription(String smsDescription) {
        this.smsDescription = smsDescription;
    }

    public boolean ispEloan() {
        return pEloan;
    }

    public void setpEloan(boolean pEloan) {
        this.pEloan = pEloan;
    }

    public String getPayrollno() {
        return Payrollno;
    }

    public void setPayrollno(String payrollno) {
        Payrollno = payrollno;
    }

    public String getLoanPurpose() {
        return LoanPurpose;
    }

    public void setLoanPurpose(String loanPurpose) {
        LoanPurpose = loanPurpose;
    }
}
