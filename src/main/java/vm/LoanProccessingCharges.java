package vm;

public class LoanProccessingCharges {
    private String loanType;

    private String code;

    private String description;

    private String calculationMode;

    private String value;

    private String accountType;

    private String accountNo;

    private String ledgeFees;

    private String insuranceCharges;

    private String minimumFees;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLoanType() {
        return loanType;
    }

    public void setLoanType(String loanType) {
        this.loanType = loanType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCalculationMode() {
        return calculationMode;
    }

    public void setCalculationMode(String calculationMode) {
        this.calculationMode = calculationMode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getLedgeFees() {
        return ledgeFees;
    }

    public void setLedgeFees(String ledgeFees) {
        this.ledgeFees = ledgeFees;
    }

    public String getInsuranceCharges() {
        return insuranceCharges;
    }

    public void setInsuranceCharges(String insuranceCharges) {
        this.insuranceCharges = insuranceCharges;
    }

    public String getMinimumFees() {
        return minimumFees;
    }

    public void setMinimumFees(String minimumFees) {
        this.minimumFees = minimumFees;
    }
}
