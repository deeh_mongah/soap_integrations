package vm;

import java.util.List;

public class GetLoanProductResponse {

    /**
     * exportLoanTypes : {"content":[{"value":{"code":"LOAN","description":"Loans","interestRate":"5.00","interestCalculationMethod":"0","insurancePercentage":"0.55","repayPeriods":"12.00","minLoanAmount":"50,000.00","maxLoanAmount":"300,000.00","loanAccount":"1200","loanInterestAccount":"4010","repaymentMethod":"Amortised","active":"Yes","interestCalculationType":"Monthly","eloan":"No","loanRepaymentType":"Installment","backofficeFrontoffice":"0","disbursementAccount":"2205"}},{"value":{"code":"SADANCE","description":"Salary Advance Loan","interestRate":"7.50","interestCalculationMethod":"0","insurancePercentage":"0.00","repayPeriods":"1.00","minLoanAmount":"5,000.00","maxLoanAmount":"35,000.00","loanAccount":"1210","loanInterestAccount":"4020","repaymentMethod":"Amortised","active":"Yes","interestCalculationType":"Daily","eloan":"No","loanRepaymentType":"Installment","backofficeFrontoffice":"Back Office","disbursementAccount":"2210"}}]}
     */

    private ExportLoanTypesBean exportLoanTypes;

    public ExportLoanTypesBean getExportLoanTypes() {
        return exportLoanTypes;
    }

    public void setExportLoanTypes(ExportLoanTypesBean exportLoanTypes) {
        this.exportLoanTypes = exportLoanTypes;
    }

    public static class ExportLoanTypesBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {
            /**
             * value : {"code":"LOAN","description":"Loans","interestRate":"5.00","interestCalculationMethod":"0","insurancePercentage":"0.55","repayPeriods":"12.00","minLoanAmount":"50,000.00","maxLoanAmount":"300,000.00","loanAccount":"1200","loanInterestAccount":"4010","repaymentMethod":"Amortised","active":"Yes","interestCalculationType":"Monthly","eloan":"No","loanRepaymentType":"Installment","backofficeFrontoffice":"0","disbursementAccount":"2205"}
             */

            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {
                /**
                 * code : LOAN
                 * description : Loans
                 * interestRate : 5.00
                 * interestCalculationMethod : 0
                 * insurancePercentage : 0.55
                 * repayPeriods : 12.00
                 * minLoanAmount : 50,000.00
                 * maxLoanAmount : 300,000.00
                 * loanAccount : 1200
                 * loanInterestAccount : 4010
                 * repaymentMethod : Amortised
                 * active : Yes
                 * interestCalculationType : Monthly
                 * eloan : No
                 * loanRepaymentType : Installment
                 * backofficeFrontoffice : 0
                 * disbursementAccount : 2205
                 */

                private String code;
                private String description;
                private String interestRate;
                private String interestCalculationMethod;
                private String insurancePercentage;
                private String repayPeriods;
                private String minLoanAmount;
                private String maxLoanAmount;
                private String loanAccount;
                private String loanInterestAccount;
                private String repaymentMethod;
                private String active;
                private String interestCalculationType;
                private String eloan;
                private String loanRepaymentType;
                private String backofficeFrontoffice;
                private String disbursementAccount;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getInterestRate() {
                    return interestRate;
                }

                public void setInterestRate(String interestRate) {
                    this.interestRate = interestRate;
                }

                public String getInterestCalculationMethod() {
                    return interestCalculationMethod;
                }

                public void setInterestCalculationMethod(String interestCalculationMethod) {
                    this.interestCalculationMethod = interestCalculationMethod;
                }

                public String getInsurancePercentage() {
                    return insurancePercentage;
                }

                public void setInsurancePercentage(String insurancePercentage) {
                    this.insurancePercentage = insurancePercentage;
                }

                public String getRepayPeriods() {
                    return repayPeriods;
                }

                public void setRepayPeriods(String repayPeriods) {
                    this.repayPeriods = repayPeriods;
                }

                public String getMinLoanAmount() {
                    return minLoanAmount;
                }

                public void setMinLoanAmount(String minLoanAmount) {
                    this.minLoanAmount = minLoanAmount;
                }

                public String getMaxLoanAmount() {
                    return maxLoanAmount;
                }

                public void setMaxLoanAmount(String maxLoanAmount) {
                    this.maxLoanAmount = maxLoanAmount;
                }

                public String getLoanAccount() {
                    return loanAccount;
                }

                public void setLoanAccount(String loanAccount) {
                    this.loanAccount = loanAccount;
                }

                public String getLoanInterestAccount() {
                    return loanInterestAccount;
                }

                public void setLoanInterestAccount(String loanInterestAccount) {
                    this.loanInterestAccount = loanInterestAccount;
                }

                public String getRepaymentMethod() {
                    return repaymentMethod;
                }

                public void setRepaymentMethod(String repaymentMethod) {
                    this.repaymentMethod = repaymentMethod;
                }

                public String getActive() {
                    return active;
                }

                public void setActive(String active) {
                    this.active = active;
                }

                public String getInterestCalculationType() {
                    return interestCalculationType;
                }

                public void setInterestCalculationType(String interestCalculationType) {
                    this.interestCalculationType = interestCalculationType;
                }

                public String getEloan() {
                    return eloan;
                }

                public void setEloan(String eloan) {
                    this.eloan = eloan;
                }

                public String getLoanRepaymentType() {
                    return loanRepaymentType;
                }

                public void setLoanRepaymentType(String loanRepaymentType) {
                    this.loanRepaymentType = loanRepaymentType;
                }

                public String getBackofficeFrontoffice() {
                    return backofficeFrontoffice;
                }

                public void setBackofficeFrontoffice(String backofficeFrontoffice) {
                    this.backofficeFrontoffice = backofficeFrontoffice;
                }

                public String getDisbursementAccount() {
                    return disbursementAccount;
                }

                public void setDisbursementAccount(String disbursementAccount) {
                    this.disbursementAccount = disbursementAccount;
                }
            }
        }
    }
}
