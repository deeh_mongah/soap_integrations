package vm;

public class LoanProducts {

    private String code;

    private String description;

    private String interestRate;

    private String interestCalculationMode;

    private String insurancePercentage;

    private String repayPeriod;

    private String minLoanAmount;

    private String maxLoanAmount;

    private String loanAccount;

    private String loanInterestAccount;

    private String repaymentMethod;

    private String Active;

    private String interestCalculationType;

    private String eLoan;

    private String  loanRepaymentType;

    private String backOfficeFrontOffice;

    private String dibursementAccount;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(String interestRate) {
        this.interestRate = interestRate;
    }

    public String getInterestCalculationMode() {
        return interestCalculationMode;
    }

    public void setInterestCalculationMode(String interestCalculationMode) {
        this.interestCalculationMode = interestCalculationMode;
    }

    public String getInsurancePercentage() {
        return insurancePercentage;
    }

    public void setInsurancePercentage(String insurancePercentage) {
        this.insurancePercentage = insurancePercentage;
    }

    public String getRepayPeriod() {
        return repayPeriod;
    }

    public void setRepayPeriod(String repayPeriod) {
        this.repayPeriod = repayPeriod;
    }

    public String getMinLoanAmount() {
        return minLoanAmount;
    }

    public void setMinLoanAmount(String minLoanAmount) {
        this.minLoanAmount = minLoanAmount;
    }

    public String getMaxLoanAmount() {
        return maxLoanAmount;
    }

    public void setMaxLoanAmount(String maxLoanAmount) {
        this.maxLoanAmount = maxLoanAmount;
    }

    public String getLoanAccount() {
        return loanAccount;
    }

    public void setLoanAccount(String loanAccount) {
        this.loanAccount = loanAccount;
    }

    public String getLoanInterestAccount() {
        return loanInterestAccount;
    }

    public void setLoanInterestAccount(String loanInterestAccount) {
        this.loanInterestAccount = loanInterestAccount;
    }

    public String getRepaymentMethod() {
        return repaymentMethod;
    }

    public void setRepaymentMethod(String repaymentMethod) {
        this.repaymentMethod = repaymentMethod;
    }

    public String getActive() {
        return Active;
    }

    public void setActive(String active) {
        Active = active;
    }

    public String getInterestCalculationType() {
        return interestCalculationType;
    }

    public void setInterestCalculationType(String interestCalculationType) {
        this.interestCalculationType = interestCalculationType;
    }

    public String geteLoan() {
        return eLoan;
    }

    public void seteLoan(String eLoan) {
        this.eLoan = eLoan;
    }

    public String getLoanRepaymentType() {
        return loanRepaymentType;
    }

    public void setLoanRepaymentType(String loanRepaymentType) {
        this.loanRepaymentType = loanRepaymentType;
    }

    public String getBackOfficeFrontOffice() {
        return backOfficeFrontOffice;
    }

    public void setBackOfficeFrontOffice(String backOfficeFrontOffice) {
        this.backOfficeFrontOffice = backOfficeFrontOffice;
    }

    public String getDibursementAccount() {
        return dibursementAccount;
    }

    public void setDibursementAccount(String dibursementAccount) {
        this.dibursementAccount = dibursementAccount;
    }
}
