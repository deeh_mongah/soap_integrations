package vm;

import java.util.List;

public class LoanChargesResponse {

    /**
     * exportLoanProcessingcharges : {"content":[{"value":{"code":"DUTY","description":"Duty"}},{"value":{"code":"INSURANCE","description":"Insurance"}}]}
     */

    private ExportLoanProcessingchargesBean exportLoanProcessingcharges;

    public ExportLoanProcessingchargesBean getExportLoanProcessingcharges() {
        return exportLoanProcessingcharges;
    }

    public void setExportLoanProcessingcharges(ExportLoanProcessingchargesBean exportLoanProcessingcharges) {
        this.exportLoanProcessingcharges = exportLoanProcessingcharges;
    }

    public static class ExportLoanProcessingchargesBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {
            /**
             * value : {"code":"DUTY","description":"Duty"}
             */

            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {
                /**
                 * code : DUTY
                 * description : Duty
                 */

                private String code;
                private String description;

                public String getCode() {
                    return code;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }
            }
        }
    }
}
