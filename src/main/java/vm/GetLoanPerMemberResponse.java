package vm;

import java.util.List;

public class GetLoanPerMemberResponse {

    /**
     * exportLoanApplications : {"content":[{"value":{"loanNo":"LN0003","applicationDate":"02/23/20","loanProductType":"LOAN","membercode":"FCL00001","companyCode":"","requestedAmount":"50,000.00","approvedAmount":"50,000.00","interest":"5.00","insurance":"0.00","memberName":"Maya Nora","installments":12,"modeOfDisbursement":"Bank Transfer","installementPeriod":"1M","repayment":"4,200.00","loanProductTypeName":"Loans","repaymentMethod":"Amortised","monthlyRepayment":"0.00","monthlyInterest":"208.33","smsDescription":"","eloan":"No","payrollNo":"","loansPurpose":""}},{"value":{"loanNo":"LN0004","applicationDate":"03/04/20","loanProductType":"SADANCE","membercode":"FCL00001","companyCode":"","requestedAmount":"5,000.00","approvedAmount":"5,000.00","interest":"7.50","insurance":"0.00","memberName":"Maya Nora","installments":45,"modeOfDisbursement":"","installementPeriod":"1M","repayment":"200.00","loanProductTypeName":"Salary Advance Loan","repaymentMethod":"Amortised","monthlyRepayment":"0.00","monthlyInterest":"31.25","smsDescription":"","eloan":"No","payrollNo":"","loansPurpose":""}}]}
     */

    private ExportLoanApplicationsBean exportLoanApplications;

    public ExportLoanApplicationsBean getExportLoanApplications() {
        return exportLoanApplications;
    }

    public void setExportLoanApplications(ExportLoanApplicationsBean exportLoanApplications) {
        this.exportLoanApplications = exportLoanApplications;
    }

    public static class ExportLoanApplicationsBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {
            /**
             * value : {"loanNo":"LN0003","applicationDate":"02/23/20","loanProductType":"LOAN","membercode":"FCL00001","companyCode":"","requestedAmount":"50,000.00","approvedAmount":"50,000.00","interest":"5.00","insurance":"0.00","memberName":"Maya Nora","installments":12,"modeOfDisbursement":"Bank Transfer","installementPeriod":"1M","repayment":"4,200.00","loanProductTypeName":"Loans","repaymentMethod":"Amortised","monthlyRepayment":"0.00","monthlyInterest":"208.33","smsDescription":"","eloan":"No","payrollNo":"","loansPurpose":""}
             */

            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {
                /**
                 * loanNo : LN0003
                 * applicationDate : 02/23/20
                 * loanProductType : LOAN
                 * membercode : FCL00001
                 * companyCode :
                 * requestedAmount : 50,000.00
                 * approvedAmount : 50,000.00
                 * interest : 5.00
                 * insurance : 0.00
                 * memberName : Maya Nora
                 * installments : 12
                 * modeOfDisbursement : Bank Transfer
                 * installementPeriod : 1M
                 * repayment : 4,200.00
                 * loanProductTypeName : Loans
                 * repaymentMethod : Amortised
                 * monthlyRepayment : 0.00
                 * monthlyInterest : 208.33
                 * smsDescription :
                 * eloan : No
                 * payrollNo :
                 * loansPurpose :
                 */

                private String loanNo;
                private String applicationDate;
                private String loanProductType;
                private String membercode;
                private String companyCode;
                private String requestedAmount;
                private String approvedAmount;
                private String interest;
                private String insurance;
                private String memberName;
                private int installments;
                private String modeOfDisbursement;
                private String installementPeriod;
                private String repayment;
                private String loanProductTypeName;
                private String repaymentMethod;
                private String monthlyRepayment;
                private String monthlyInterest;
                private String smsDescription;
                private String eloan;
                private String payrollNo;
                private String loansPurpose;

                public String getLoanNo() {
                    return loanNo;
                }

                public void setLoanNo(String loanNo) {
                    this.loanNo = loanNo;
                }

                public String getApplicationDate() {
                    return applicationDate;
                }

                public void setApplicationDate(String applicationDate) {
                    this.applicationDate = applicationDate;
                }

                public String getLoanProductType() {
                    return loanProductType;
                }

                public void setLoanProductType(String loanProductType) {
                    this.loanProductType = loanProductType;
                }

                public String getMembercode() {
                    return membercode;
                }

                public void setMembercode(String membercode) {
                    this.membercode = membercode;
                }

                public String getCompanyCode() {
                    return companyCode;
                }

                public void setCompanyCode(String companyCode) {
                    this.companyCode = companyCode;
                }

                public String getRequestedAmount() {
                    return requestedAmount;
                }

                public void setRequestedAmount(String requestedAmount) {
                    this.requestedAmount = requestedAmount;
                }

                public String getApprovedAmount() {
                    return approvedAmount;
                }

                public void setApprovedAmount(String approvedAmount) {
                    this.approvedAmount = approvedAmount;
                }

                public String getInterest() {
                    return interest;
                }

                public void setInterest(String interest) {
                    this.interest = interest;
                }

                public String getInsurance() {
                    return insurance;
                }

                public void setInsurance(String insurance) {
                    this.insurance = insurance;
                }

                public String getMemberName() {
                    return memberName;
                }

                public void setMemberName(String memberName) {
                    this.memberName = memberName;
                }

                public int getInstallments() {
                    return installments;
                }

                public void setInstallments(int installments) {
                    this.installments = installments;
                }

                public String getModeOfDisbursement() {
                    return modeOfDisbursement;
                }

                public void setModeOfDisbursement(String modeOfDisbursement) {
                    this.modeOfDisbursement = modeOfDisbursement;
                }

                public String getInstallementPeriod() {
                    return installementPeriod;
                }

                public void setInstallementPeriod(String installementPeriod) {
                    this.installementPeriod = installementPeriod;
                }

                public String getRepayment() {
                    return repayment;
                }

                public void setRepayment(String repayment) {
                    this.repayment = repayment;
                }

                public String getLoanProductTypeName() {
                    return loanProductTypeName;
                }

                public void setLoanProductTypeName(String loanProductTypeName) {
                    this.loanProductTypeName = loanProductTypeName;
                }

                public String getRepaymentMethod() {
                    return repaymentMethod;
                }

                public void setRepaymentMethod(String repaymentMethod) {
                    this.repaymentMethod = repaymentMethod;
                }

                public String getMonthlyRepayment() {
                    return monthlyRepayment;
                }

                public void setMonthlyRepayment(String monthlyRepayment) {
                    this.monthlyRepayment = monthlyRepayment;
                }

                public String getMonthlyInterest() {
                    return monthlyInterest;
                }

                public void setMonthlyInterest(String monthlyInterest) {
                    this.monthlyInterest = monthlyInterest;
                }

                public String getSmsDescription() {
                    return smsDescription;
                }

                public void setSmsDescription(String smsDescription) {
                    this.smsDescription = smsDescription;
                }

                public String getEloan() {
                    return eloan;
                }

                public void setEloan(String eloan) {
                    this.eloan = eloan;
                }

                public String getPayrollNo() {
                    return payrollNo;
                }

                public void setPayrollNo(String payrollNo) {
                    this.payrollNo = payrollNo;
                }

                public String getLoansPurpose() {
                    return loansPurpose;
                }

                public void setLoansPurpose(String loansPurpose) {
                    this.loansPurpose = loansPurpose;
                }
            }
        }
    }
}
