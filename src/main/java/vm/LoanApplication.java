package vm;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;

public class LoanApplication {


    protected Date ApplicationDate;

    protected String LoanProductType;

    protected String pMemberCode;


    protected String pLoanNo;

    protected String CompanyCode;

    protected BigDecimal  RequestedAmount;

    protected BigDecimal  ApprovedAmount;

    protected BigDecimal  Interest;

    protected BigDecimal  Insurance;

    protected String MemberName;

    protected int Installments;

    protected int ModeOfDisbursement;

    protected String InstallementPeriod;

    protected BigDecimal  Repayment;

    protected String LoanProductTypeName;

    protected int RepaymentMethod;

    protected BigDecimal  MonthlyRepayment;

    protected BigDecimal  MonthlyInterest;

    protected String SmsDescription;

    protected boolean Eloan;

    protected String PayrollNo;

    protected String LoansPurpose;


    public String getpLoanNo() {
        return pLoanNo;
    }

    public void setpLoanNo(String pLoanNo) {
        this.pLoanNo = pLoanNo;
    }

    public Date getApplicationDate() {
        return ApplicationDate;
    }

    public void setApplicationDate(Date applicationDate) {
        ApplicationDate = applicationDate;
    }

    public String getLoanProductType() {
        return LoanProductType;
    }

    public void setLoanProductType(String loanProductType) {
        LoanProductType = loanProductType;
    }

    public String getpMemberCode() {
        return pMemberCode;
    }

    public void setpMemberCode(String pMemberCode) {
        this.pMemberCode = pMemberCode;
    }

    public String getCompanyCode() {
        return CompanyCode;
    }

    public void setCompanyCode(String companyCode) {
        CompanyCode = companyCode;
    }

    public BigDecimal getRequestedAmount() {
        return RequestedAmount;
    }

    public void setRequestedAmount(BigDecimal requestedAmount) {
        RequestedAmount = requestedAmount;
    }

    public BigDecimal getApprovedAmount() {
        return ApprovedAmount;
    }

    public void setApprovedAmount(BigDecimal approvedAmount) {
        ApprovedAmount = approvedAmount;
    }

    public BigDecimal getInterest() {
        return Interest;
    }

    public void setInterest(BigDecimal interest) {
        Interest = interest;
    }

    public BigDecimal getInsurance() {
        return Insurance;
    }

    public void setInsurance(BigDecimal insurance) {
        Insurance = insurance;
    }

    public String getMemberName() {
        return MemberName;
    }

    public void setMemberName(String memberName) {
        MemberName = memberName;
    }

    public int getInstallments() {
        return Installments;
    }

    public void setInstallments(int installments) {
        Installments = installments;
    }

    public int getModeOfDisbursement() {
        return ModeOfDisbursement;
    }

    public void setModeOfDisbursement(int modeOfDisbursement) {
        ModeOfDisbursement = modeOfDisbursement;
    }

    public String getInstallementPeriod() {
        return InstallementPeriod;
    }

    public void setInstallementPeriod(String installementPeriod) {
        InstallementPeriod = installementPeriod;
    }

    public BigDecimal getRepayment() {
        return Repayment;
    }

    public void setRepayment(BigDecimal repayment) {
        Repayment = repayment;
    }

    public String getLoanProductTypeName() {
        return LoanProductTypeName;
    }

    public void setLoanProductTypeName(String loanProductTypeName) {
        LoanProductTypeName = loanProductTypeName;
    }

    public int getRepaymentMethod() {
        return RepaymentMethod;
    }

    public void setRepaymentMethod(int repaymentMethod) {
        RepaymentMethod = repaymentMethod;
    }

    public BigDecimal getMonthlyRepayment() {
        return MonthlyRepayment;
    }

    public void setMonthlyRepayment(BigDecimal monthlyRepayment) {
        MonthlyRepayment = monthlyRepayment;
    }

    public BigDecimal getMonthlyInterest() {
        return MonthlyInterest;
    }

    public void setMonthlyInterest(BigDecimal monthlyInterest) {
        MonthlyInterest = monthlyInterest;
    }

    public String getSmsDescription() {
        return SmsDescription;
    }

    public void setSmsDescription(String smsDescription) {
        SmsDescription = smsDescription;
    }

    public boolean isEloan() {
        return Eloan;
    }

    public void setEloan(boolean eloan) {
        Eloan = eloan;
    }

    public String getPayrollNo() {
        return PayrollNo;
    }

    public void setPayrollNo(String payrollNo) {
        PayrollNo = payrollNo;
    }

    public String getLoansPurpose() {
        return LoansPurpose;
    }

    public void setLoansPurpose(String loansPurpose) {
        LoansPurpose = loansPurpose;
    }
}
