package vm;


import java.util.List;

public class MemberDetailResponse {


    /**
     * exportMemberDetails : {"content":[{"value":{"memberNo":"FCL00001","memberName":"Maya Nora Ang'asa","mobilePhoneNo":"0720915686","email":"shem.a.ayienda@gmail.com","dateofBirth":"02/09/13","companyCode":"007","companyName":"Nine Star Ins Agency","idno":"232443"}}]}
     */

    private ExportMemberDetailsBean exportMemberDetails;

    public ExportMemberDetailsBean getExportMemberDetails() {
        return exportMemberDetails;
    }

    public void setExportMemberDetails(ExportMemberDetailsBean exportMemberDetails) {
        this.exportMemberDetails = exportMemberDetails;
    }

    public static class ExportMemberDetailsBean {
        private List<ContentBean> content;

        public List<ContentBean> getContent() {
            return content;
        }

        public void setContent(List<ContentBean> content) {
            this.content = content;
        }

        public static class ContentBean {
            /**
             * value : {"memberNo":"FCL00001","memberName":"Maya Nora Ang'asa","mobilePhoneNo":"0720915686","email":"shem.a.ayienda@gmail.com","dateofBirth":"02/09/13","companyCode":"007","companyName":"Nine Star Ins Agency","idno":"232443"}
             */

            private ValueBean value;

            public ValueBean getValue() {
                return value;
            }

            public void setValue(ValueBean value) {
                this.value = value;
            }

            public static class ValueBean {
                /**
                 * memberNo : FCL00001
                 * memberName : Maya Nora Ang'asa
                 * mobilePhoneNo : 0720915686
                 * email : shem.a.ayienda@gmail.com
                 * dateofBirth : 02/09/13
                 * companyCode : 007
                 * companyName : Nine Star Ins Agency
                 * idno : 232443
                 */

                private String memberNo;
                private String memberName;
                private String mobilePhoneNo;
                private String email;
                private String dateofBirth;
                private String companyCode;
                private String companyName;
                private String idno;

                public String getMemberNo() {
                    return memberNo;
                }

                public void setMemberNo(String memberNo) {
                    this.memberNo = memberNo;
                }

                public String getMemberName() {
                    return memberName;
                }

                public void setMemberName(String memberName) {
                    this.memberName = memberName;
                }

                public String getMobilePhoneNo() {
                    return mobilePhoneNo;
                }

                public void setMobilePhoneNo(String mobilePhoneNo) {
                    this.mobilePhoneNo = mobilePhoneNo;
                }

                public String getEmail() {
                    return email;
                }

                public void setEmail(String email) {
                    this.email = email;
                }

                public String getDateofBirth() {
                    return dateofBirth;
                }

                public void setDateofBirth(String dateofBirth) {
                    this.dateofBirth = dateofBirth;
                }

                public String getCompanyCode() {
                    return companyCode;
                }

                public void setCompanyCode(String companyCode) {
                    this.companyCode = companyCode;
                }

                public String getCompanyName() {
                    return companyName;
                }

                public void setCompanyName(String companyName) {
                    this.companyName = companyName;
                }

                public String getIdno() {
                    return idno;
                }

                public void setIdno(String idno) {
                    this.idno = idno;
                }
            }
        }
    }
}
